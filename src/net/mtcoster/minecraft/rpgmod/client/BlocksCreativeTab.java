package net.mtcoster.minecraft.rpgmod.client;

import net.minecraft.creativetab.CreativeTabs;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;

public class BlocksCreativeTab extends CreativeTabs {
    public BlocksCreativeTab(String label) {
        super(label);
    }

    @Override
    public int getTabIconItemIndex() {
        return BlockInfo.IRON_DIAMOND_ID;
    }

    @Override
    public String getTabLabel() {
        return ModInfo.TAB_BLOCKS_NAME;
    }
}
