package net.mtcoster.minecraft.rpgmod.client;

import net.minecraft.creativetab.CreativeTabs;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;

public class RpgModCreativeTab extends CreativeTabs {
    public RpgModCreativeTab(String label) {
        super(label);
    }

    @Override
    public int getTabIconItemIndex() {
        return BlockInfo.TOTEM_DIRT_ID;
    }

    @Override
    public String getTabLabel() {
        return ModInfo.TAB_RPGMOD_NAME;
    }
}
