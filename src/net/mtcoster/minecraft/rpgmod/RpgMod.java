package net.mtcoster.minecraft.rpgmod;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.mtcoster.minecraft.rpgmod.blocks.Blocks;
import net.mtcoster.minecraft.rpgmod.client.BlocksCreativeTab;
import net.mtcoster.minecraft.rpgmod.client.RpgModCreativeTab;
import net.mtcoster.minecraft.rpgmod.items.Items;
import net.mtcoster.minecraft.rpgmod.network.PacketHandler;
import net.mtcoster.minecraft.rpgmod.proxies.CommonProxy;

import java.util.logging.Level;
import java.util.logging.Logger;

@Mod(modid = ModInfo.ID, name = ModInfo.NAME, version = ModInfo.VERSION)
@NetworkMod(clientSideRequired = true, serverSideRequired = true, channels = {ModInfo.NETWORK_CHANNEL}, packetHandler = PacketHandler.class)
public class RpgMod {
    @Mod.Instance(ModInfo.ID)
    public static RpgMod instance;

    @SidedProxy(clientSide = "net.mtcoster.minecraft.rpgmod.proxies.ClientProxy", serverSide = "net.mtcoster.minecraft.rpgmod.proxies.CommonProxy")
    public static CommonProxy proxy;

    public Logger logger = Logger.getLogger(ModInfo.ID);
    public CreativeTabs tabRpgMod;
    public CreativeTabs tabBlocks;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent preInitEvent) {
        ConfigHandler.init(preInitEvent.getSuggestedConfigurationFile());

        tabRpgMod = new RpgModCreativeTab(ModInfo.TAB_RPGMOD_ID);
        tabBlocks = new BlocksCreativeTab(ModInfo.TAB_BLOCKS_ID);

        LanguageRegistry.instance().addStringLocalization("itemGroup." + ModInfo.TAB_RPGMOD_NAME, ModInfo.TAB_RPGMOD_NAME);
        LanguageRegistry.instance().addStringLocalization("itemGroup." + ModInfo.TAB_BLOCKS_NAME, ModInfo.TAB_BLOCKS_NAME);

        Items.initItems();
        Blocks.initBlocks();

        logger.log(Level.FINE, "Pre-init completed");
    }

    @Mod.EventHandler
    public void load(FMLInitializationEvent loadEvent) {
        Items.registerCrafting();
        Items.registerLang();

        Blocks.registerCrafting();
        Blocks.registerLang();

        logger.log(Level.FINE, "Load completed");
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent postInitEvent) {
        logger.log(Level.FINE, "Post-init completed");
    }
}
