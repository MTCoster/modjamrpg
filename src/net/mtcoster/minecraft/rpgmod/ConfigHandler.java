package net.mtcoster.minecraft.rpgmod;

import net.minecraftforge.common.Configuration;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.ItemInfo;

import java.io.File;

public class ConfigHandler {
    public static final String CATEGORY_ITEM_IDS = "Item IDs";
    public static final String CATEGORY_BLOCK_IDS = "Block IDs";

    public static void init(File file){
        Configuration config = new Configuration(file);

        config.load();

        ItemInfo.TALISMAN_INERT_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_INERT_UNLOCAL_NAME, ItemInfo.TALISMAN_INERT_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_DIRT_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_DIRT_UNLOCAL_NAME, ItemInfo.TALISMAN_DIRT_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_ENDER_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_ENDER_UNLOCAL_NAME, ItemInfo.TALISMAN_ENDER_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_FARMING_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_FARMING_UNLOCAL_NAME, ItemInfo.TALISMAN_FARMING_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_HEAL_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_HEAL_UNLOCAL_NAME, ItemInfo.TALISMAN_HEAL_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_INVISIBLE_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_INVISIBLE_UNLOCAL_NAME, ItemInfo.TALISMAN_INVISIBLE_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_NETHER_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_NETHER_UNLOCAL_NAME, ItemInfo.TALISMAN_NETHER_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_NIGHT_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_NIGHT_UNLOCAL_NAME, ItemInfo.TALISMAN_NIGHT_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_REGEN_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_REGEN_UNLOCAL_NAME, ItemInfo.TALISMAN_REGEN_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_RESISTANCE_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_RESISTANCE_UNLOCAL_NAME, ItemInfo.TALISMAN_RESISTANCE_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_SPEED_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_SPEED_UNLOCAL_NAME, ItemInfo.TALISMAN_SPEED_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_STRENGTH_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_STRENGTH_UNLOCAL_NAME, ItemInfo.TALISMAN_STRENGTH_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_WATER_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_WATER_UNLOCAL_NAME, ItemInfo.TALISMAN_WATER_DEFAULT_ID).getInt() - 256;
        ItemInfo.TALISMAN_WITHER_ID = config.getItem(CATEGORY_ITEM_IDS, ItemInfo.TALISMAN_WITHER_UNLOCAL_NAME, ItemInfo.TALISMAN_WITHER_DEFAULT_ID).getInt() - 256;

        BlockInfo.TOTEM_DIRT_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_DIRT_UNLOCAL_NAME, BlockInfo.TOTEM_DIRT_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_ENDER_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_ENDER_UNLOCAL_NAME, BlockInfo.TOTEM_ENDER_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_FARMING_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_FARMING_UNLOCAL_NAME, BlockInfo.TOTEM_FARMING_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_HEAL_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_HEAL_UNLOCAL_NAME, BlockInfo.TOTEM_HEAL_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_INVISIBLE_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_INVISIBLE_UNLOCAL_NAME, BlockInfo.TOTEM_INVISIBLE_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_NETHER_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_NETHER_UNLOCAL_NAME, BlockInfo.TOTEM_NETHER_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_NIGHT_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_NIGHT_UNLOCAL_NAME, BlockInfo.TOTEM_NIGHT_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_REGEN_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_REGEN_UNLOCAL_NAME, BlockInfo.TOTEM_REGEN_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_RESISTANCE_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_RESISTANCE_UNLOCAL_NAME, BlockInfo.TOTEM_RESISTANCE_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_SPEED_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_SPEED_UNLOCAL_NAME, BlockInfo.TOTEM_SPEED_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_STRENGTH_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_STRENGTH_UNLOCAL_NAME, BlockInfo.TOTEM_STRENGTH_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_WATER_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_WATER_UNLOCAL_NAME, BlockInfo.TOTEM_WATER_DEFAULT_ID).getInt();
        BlockInfo.TOTEM_WITHER_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.TOTEM_WITHER_UNLOCAL_NAME, BlockInfo.TOTEM_WITHER_DEFAULT_ID).getInt();

        BlockInfo.COMPRESSED_REEDS_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.COMPRESSED_REEDS_UNLOCAL_NAME, BlockInfo.COMPRESSED_REEDS_DEFAULT_ID).getInt();
        BlockInfo.APPLE_CRATE_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.APPLE_CRATE_UNLOCAL_NAME, BlockInfo.APPLE_CRATE_DEFAULT_ID).getInt();

        BlockInfo.IRON_DIAMOND_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.IRON_DIAMOND_UNLOCAL_NAME, BlockInfo.IRON_DIAMOND_DEFAULT_ID).getInt();

        BlockInfo.STAIRS_COAL_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.STAIRS_COAL_UNLOCAL_NAME, BlockInfo.STAIRS_COAL_DEFAULT_ID).getInt();
        BlockInfo.STAIRS_DIAMOND_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.STAIRS_DIAMOND_UNLOCAL_NAME, BlockInfo.STAIRS_DIAMOND_DEFAULT_ID).getInt();
        BlockInfo.STAIRS_EMERALD_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.STAIRS_EMERALD_UNLOCAL_NAME, BlockInfo.STAIRS_EMERALD_DEFAULT_ID).getInt();
        BlockInfo.STAIRS_GOLD_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.STAIRS_GOLD_UNLOCAL_NAME, BlockInfo.STAIRS_GOLD_DEFAULT_ID).getInt();
        BlockInfo.STAIRS_IRON_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.STAIRS_IRON_UNLOCAL_NAME, BlockInfo.STAIRS_IRON_DEFAULT_ID).getInt();
        BlockInfo.STAIRS_LAPIS_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.STAIRS_LAPIS_UNLOCAL_NAME, BlockInfo.STAIRS_LAPIS_DEFAULT_ID).getInt();
        BlockInfo.STAIRS_REDSTONE_ID = config.getBlock(CATEGORY_BLOCK_IDS, BlockInfo.STAIRS_REDSTONE_UNLOCAL_NAME, BlockInfo.STAIRS_REDSTONE_DEFAULT_ID).getInt();

        config.save();
    }
}
