package net.mtcoster.minecraft.rpgmod.blocks.blocks;

import net.minecraft.block.Block;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;

public class BlockCoalStairs extends BlockBlocksStairs {
    public BlockCoalStairs(int id) {
        super(id, Block.field_111034_cE);
        setUnlocalizedName(BlockInfo.STAIRS_COAL_UNLOCAL_NAME);
    }
}
