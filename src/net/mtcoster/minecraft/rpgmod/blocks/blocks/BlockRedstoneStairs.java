package net.mtcoster.minecraft.rpgmod.blocks.blocks;

import net.minecraft.block.Block;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;

public class BlockRedstoneStairs extends BlockBlocksStairs {
    public BlockRedstoneStairs(int id) {
        super(id, Block.blockRedstone);
        setUnlocalizedName(BlockInfo.STAIRS_REDSTONE_UNLOCAL_NAME);
    }
}
