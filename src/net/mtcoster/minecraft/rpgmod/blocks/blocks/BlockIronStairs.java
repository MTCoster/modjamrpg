package net.mtcoster.minecraft.rpgmod.blocks.blocks;

import net.minecraft.block.Block;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;

public class BlockIronStairs extends BlockBlocksStairs {
    public BlockIronStairs(int id) {
        super(id, Block.blockIron);
        setUnlocalizedName(BlockInfo.STAIRS_IRON_UNLOCAL_NAME);
    }
}
