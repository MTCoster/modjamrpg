package net.mtcoster.minecraft.rpgmod.blocks.blocks;

import net.minecraft.block.Block;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;

public class BlockGoldStairs extends BlockBlocksStairs {
    public BlockGoldStairs(int id) {
        super(id, Block.blockGold);
        setUnlocalizedName(BlockInfo.STAIRS_GOLD_UNLOCAL_NAME);
    }
}
