package net.mtcoster.minecraft.rpgmod.blocks.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;

public class BlockIronDiamond extends Block {
    public BlockIronDiamond(int id) {
        super(id, Material.iron);
        setUnlocalizedName(BlockInfo.IRON_DIAMOND_UNLOCAL_NAME);
        setCreativeTab(CreativeTabs.tabBlock);
        setHardness(6.0f);
        setResistance(12.0f);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.IRON_DIAMOND_TEXTURE);
    }
}
