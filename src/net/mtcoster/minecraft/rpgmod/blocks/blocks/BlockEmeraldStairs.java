package net.mtcoster.minecraft.rpgmod.blocks.blocks;

import net.minecraft.block.Block;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;

public class BlockEmeraldStairs extends BlockBlocksStairs {
    public BlockEmeraldStairs(int id) {
        super(id, Block.blockEmerald);
        setUnlocalizedName(BlockInfo.STAIRS_EMERALD_UNLOCAL_NAME);
    }
}
