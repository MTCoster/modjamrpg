package net.mtcoster.minecraft.rpgmod.blocks.blocks;

import net.minecraft.block.Block;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;

public class BlockLapisStairs extends BlockBlocksStairs {
    public BlockLapisStairs(int id) {
        super(id, Block.blockLapis);
        setUnlocalizedName(BlockInfo.STAIRS_LAPIS_UNLOCAL_NAME);
    }
}
