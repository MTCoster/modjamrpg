package net.mtcoster.minecraft.rpgmod.blocks.blocks;

import net.minecraft.block.Block;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;

public class BlockDiamondStairs extends BlockBlocksStairs {
    public BlockDiamondStairs(int id) {
        super(id, Block.blockDiamond);
        setUnlocalizedName(BlockInfo.STAIRS_DIAMOND_UNLOCAL_NAME);
    }
}
