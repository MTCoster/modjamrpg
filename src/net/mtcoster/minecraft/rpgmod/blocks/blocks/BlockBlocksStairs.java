package net.mtcoster.minecraft.rpgmod.blocks.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;
import net.mtcoster.minecraft.rpgmod.RpgMod;

public abstract class BlockBlocksStairs extends BlockStairs {
    protected BlockBlocksStairs(int id, Block block) {
        super(id, block, 0);
        setCreativeTab(RpgMod.instance.tabBlocks);
    }
}
