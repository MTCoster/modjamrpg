package net.mtcoster.minecraft.rpgmod.blocks;

public class BlockInfo {
    public static int TOTEM_DIRT_ID;
    public static final int TOTEM_DIRT_DEFAULT_ID = 2500;
    public static final String TOTEM_DIRT_TEXTURE = "totem_dirt";
    public static final String TOTEM_DIRT_UNLOCAL_NAME = "DirtTotemCore";
    public static final String TOTEM_DIRT_NAME = "Totem of the Earth (Core)";

    public static int TOTEM_ENDER_ID;
    public static final int TOTEM_ENDER_DEFAULT_ID = 2501;
    public static final String TOTEM_ENDER_TEXTURE = "totem_ender";
    public static final String TOTEM_ENDER_UNLOCAL_NAME = "EnderTotemCore";
    public static final String TOTEM_ENDER_NAME = "Totem of the End (Core)";

    public static int TOTEM_FARMING_ID;
    public static final int TOTEM_FARMING_DEFAULT_ID = 2502;
    public static final String TOTEM_FARMING_TEXTURE = "totem_farming";
    public static final String TOTEM_FARMING_UNLOCAL_NAME = "FarmingTotemCore";
    public static final String TOTEM_FARMING_NAME = "Totem of Nourishment (Core)";

    public static int TOTEM_HEAL_ID;
    public static final int TOTEM_HEAL_DEFAULT_ID = 2503;
    public static final String TOTEM_HEAL_TEXTURE = "totem_heal";
    public static final String TOTEM_HEAL_UNLOCAL_NAME = "HealTotemCore";
    public static final String TOTEM_HEAL_NAME = "Totem of Healing (Core)";

    public static int TOTEM_INVISIBLE_ID;
    public static final int TOTEM_INVISIBLE_DEFAULT_ID = 2504;
    public static final String TOTEM_INVISIBLE_TEXTURE = "totem_invisible";
    public static final String TOTEM_INVISIBLE_UNLOCAL_NAME = "InvisibleTotemCore";
    public static final String TOTEM_INVISIBLE_NAME = "Totem of Vanishing (Core)";

    public static int TOTEM_NETHER_ID;
    public static final int TOTEM_NETHER_DEFAULT_ID = 2505;
    public static final String TOTEM_NETHER_TEXTURE = "totem_nether";
    public static final String TOTEM_NETHER_UNLOCAL_NAME = "NetherTotemCore";
    public static final String TOTEM_NETHER_NAME = "Totem of Inflammability (Core)";

    public static int TOTEM_NIGHT_ID;
    public static final int TOTEM_NIGHT_DEFAULT_ID = 2506;
    public static final String TOTEM_NIGHT_TEXTURE = "totem_night";
    public static final String TOTEM_NIGHT_UNLOCAL_NAME = "NightTotemCore";
    public static final String TOTEM_NIGHT_NAME = "Totem of the Night (Core)";

    public static int TOTEM_REGEN_ID;
    public static final int TOTEM_REGEN_DEFAULT_ID = 2507;
    public static final String TOTEM_REGEN_TEXTURE = "totem_regen";
    public static final String TOTEM_REGEN_UNLOCAL_NAME = "RegenTotemCore";
    public static final String TOTEM_REGEN_NAME = "Totem of Regeneration (Core)";

    public static int TOTEM_RESISTANCE_ID;
    public static final int TOTEM_RESISTANCE_DEFAULT_ID = 2508;
    public static final String TOTEM_RESISTANCE_TEXTURE = "totem_resistance";
    public static final String TOTEM_RESISTANCE_UNLOCAL_NAME = "ResistanceTotemCore";
    public static final String TOTEM_RESISTANCE_NAME = "Totem of Invincibility \u00a78(sort of)\u00a7f (Core)";

    public static int TOTEM_SPEED_ID;
    public static final int TOTEM_SPEED_DEFAULT_ID = 2509;
    public static final String TOTEM_SPEED_TEXTURE = "totem_speed";
    public static final String TOTEM_SPEED_UNLOCAL_NAME = "SpeedTotemCore";
    public static final String TOTEM_SPEED_NAME = "Totem of Acceleration (Core)";

    public static int TOTEM_STRENGTH_ID;
    public static final int TOTEM_STRENGTH_DEFAULT_ID = 2510;
    public static final String TOTEM_STRENGTH_TEXTURE = "totem_strength";
    public static final String TOTEM_STRENGTH_UNLOCAL_NAME = "StrengthTotemCore";
    public static final String TOTEM_STRENGTH_NAME = "Totem of Strength (Core)";

    public static int TOTEM_WATER_ID;
    public static final int TOTEM_WATER_DEFAULT_ID = 2511;
    public static final String TOTEM_WATER_TEXTURE = "totem_water";
    public static final String TOTEM_WATER_UNLOCAL_NAME = "WaterTotemCore";
    public static final String TOTEM_WATER_NAME = "Totem of Gillification (Core)";

    public static int TOTEM_WITHER_ID;
    public static final int TOTEM_WITHER_DEFAULT_ID = 2512;
    public static final String TOTEM_WITHER_TEXTURE = "totem_wither";
    public static final String TOTEM_WITHER_UNLOCAL_NAME = "WitherTotemCore";
    public static final String TOTEM_WITHER_NAME = "Totem of the Wither (Core)";

    public static int COMPRESSED_REEDS_ID;
    public static final int COMPRESSED_REEDS_DEFAULT_ID = 2520;
    public static final String COMPRESSED_REEDS_SIDE_TEXTURE = "compressed_reeds_side";
    public static final String COMPRESSED_REEDS_TOP_TEXTURE = "compressed_reeds_top";
    public static final String COMPRESSED_REEDS_UNLOCAL_NAME = "CompressedReeds";
    public static final String COMPRESSED_REEDS_NAME = "Compressed Reeds";

    public static int APPLE_CRATE_ID;
    public static final int APPLE_CRATE_DEFAULT_ID = 2521;
    public static final String APPLE_CRATE_SIDE_TEXTURE = "apple_crate";
    public static final String APPLE_CRATE_TOP_TEXTURE = "apple_crate_top";
    public static final String APPLE_CRATE_BOTTOM_TEXTURE = "apple_crate_bottom";
    public static final String APPLE_CRATE_UNLOCAL_NAME = "AppleCrate";
    public static final String APPLE_CRATE_NAME = "Crate of Apples";

    public static int IRON_DIAMOND_ID;
    public static final int IRON_DIAMOND_DEFAULT_ID = 2530;
    public static final String IRON_DIAMOND_TEXTURE = "iron_diamond";
    public static final String IRON_DIAMOND_UNLOCAL_NAME = "IronDiamond";
    public static final String IRON_DIAMOND_NAME = "Diamantine Iron";

    public static int STAIRS_COAL_ID;
    public static final int STAIRS_COAL_DEFAULT_ID = 2540;
    public static final String STAIRS_COAL_UNLOCAL_NAME = "CoalStairs";
    public static final String STAIRS_COAL_NAME = "Coal Stairs";

    public static int STAIRS_DIAMOND_ID;
    public static final int STAIRS_DIAMOND_DEFAULT_ID = 2541;
    public static final String STAIRS_DIAMOND_UNLOCAL_NAME = "DiamondStairs";
    public static final String STAIRS_DIAMOND_NAME = "Diamond Stairs";

    public static int STAIRS_EMERALD_ID;
    public static final int STAIRS_EMERALD_DEFAULT_ID = 2542;
    public static final String STAIRS_EMERALD_UNLOCAL_NAME = "EmeraldStairs";
    public static final String STAIRS_EMERALD_NAME = "Emerald Stairs";

    public static int STAIRS_IRON_ID;
    public static final int STAIRS_IRON_DEFAULT_ID = 2543;
    public static final String STAIRS_IRON_UNLOCAL_NAME = "IronStairs";
    public static final String STAIRS_IRON_NAME = "Iron Stairs";

    public static int STAIRS_GOLD_ID;
    public static final int STAIRS_GOLD_DEFAULT_ID = 2544;
    public static final String STAIRS_GOLD_UNLOCAL_NAME = "GoldStairs";
    public static final String STAIRS_GOLD_NAME = "Gold Stairs";

    public static int STAIRS_LAPIS_ID;
    public static final int STAIRS_LAPIS_DEFAULT_ID = 2545;
    public static final String STAIRS_LAPIS_UNLOCAL_NAME = "LapisStairs";
    public static final String STAIRS_LAPIS_NAME = "Lapis Stairs";

    public static int STAIRS_REDSTONE_ID;
    public static final int STAIRS_REDSTONE_DEFAULT_ID = 2546;
    public static final String STAIRS_REDSTONE_UNLOCAL_NAME = "RedstoneStairs";
    public static final String STAIRS_REDSTONE_NAME = "Redstone Stairs";


}
