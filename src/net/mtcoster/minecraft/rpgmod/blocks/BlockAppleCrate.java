package net.mtcoster.minecraft.rpgmod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.Icon;
import net.mtcoster.minecraft.rpgmod.ModInfo;

public class BlockAppleCrate extends Block {
    public BlockAppleCrate(int id) {
        super(id, Material.wood);
        setCreativeTab(CreativeTabs.tabFood);
        setUnlocalizedName(BlockInfo.APPLE_CRATE_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    public Icon topIcon;
    @SideOnly(Side.CLIENT)
    public Icon sideIcon;
    @SideOnly(Side.CLIENT)
    public Icon bottomIcon;

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        topIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.APPLE_CRATE_TOP_TEXTURE);
        sideIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.APPLE_CRATE_SIDE_TEXTURE);
        bottomIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.APPLE_CRATE_BOTTOM_TEXTURE);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public Icon getIcon(int side, int meta) {
        if(side == 1){
            return topIcon;
        }else if(side == 0){
            return bottomIcon;
        }else{
            return sideIcon;
        }
    }
}
