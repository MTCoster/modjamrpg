
package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreFarming extends TotemCore {
    public TotemCoreFarming(int id, Material material) {
        super(id, material, true);

        structure = new Structure[]{
                new Structure(-1, -1, -1, Block.tilledField.blockID),
                new Structure(-1, -1,  1, Block.tilledField.blockID),
                new Structure( 0, -1, -1, Block.field_111038_cB.blockID), // Hay bales
                new Structure( 0,  0, -1, Block.field_111038_cB.blockID),
                new Structure( 0,  0,  1, Block.field_111038_cB.blockID),
                new Structure( 0, -1,  1, Block.field_111038_cB.blockID),
                new Structure(-1, -1,  0, Block.fenceGate.blockID),
                new Structure( 0, -1,  0, Block.waterStill.blockID)
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.potato, 48),
                new ItemStack(Item.carrot, 48),
                new ItemStack(Item.seeds, 48),
                new ItemStack(Item.pumpkinSeeds, 16),
                new ItemStack(Item.melonSeeds, 16),
                new ItemStack(Item.wheat, 32),
        };

        talisman = Items.talismanFarming;

        potionID = 23;
        potionTime = 4;
        levelsTaken = 5;
        numLevels = 4;
        timeIncrement = 0.5f;

        setUnlocalizedName(BlockInfo.TOTEM_FARMING_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_FARMING_TEXTURE);
    }
    @Override
    public int getRenderBlockPass() {
        return 1;
    }
}
