package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreHeal extends TotemCore {
    public TotemCoreHeal(int id, Material material) {
        super(id, material,true);

        structure = new Structure[] {
                new Structure( 0,  0, -1, Block.melon.blockID),
                new Structure( 0, -1,  0, Block.melon.blockID),
                new Structure( 0,  0,  1, Block.melon.blockID),
                new Structure( 0,  1, -1, Block.pumpkin.blockID),
                new Structure( 0,  1,  0, Block.pumpkin.blockID),
                new Structure( 0,  1,  1, Block.pumpkin.blockID),
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.appleGold, 2, 0)
        };

        talisman = Items.talismanHeal;

        potionID = 6;
        potionTime = 5;
        levelsTaken = 5;
        numLevels = 5;

        setUnlocalizedName(BlockInfo.TOTEM_HEAL_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_HEAL_TEXTURE);
    }
}
