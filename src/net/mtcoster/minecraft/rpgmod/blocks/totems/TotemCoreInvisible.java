package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreInvisible extends TotemCore {
    public TotemCoreInvisible(int id, Material material) {
        super(id, material,true);

        structure = new Structure[] {
                new Structure( 0, -1,  0, Block.obsidian.blockID),
                new Structure(-1, -1, -1, Block.cloth.blockID),
                new Structure( 0, -1, -1, Block.cloth.blockID),
                new Structure( 0, -1,  1, Block.cloth.blockID),
                new Structure(-1, -1,  1, Block.cloth.blockID),
                new Structure( 0,  1, -1, Block.field_111031_cC.blockID),
                new Structure( 0,  1,  1, Block.field_111031_cC.blockID),
                new Structure( 0,  0, -1, Block.bookShelf.blockID),
                new Structure( 0,  0,  1, Block.bookShelf.blockID),
                new Structure( 0,  1,  0, Block.enchantmentTable.blockID)
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.book, 32)
        };

        talisman = Items.talismanInvisible;

        potionID = 14;
        potionTime = 2400;
        levelsTaken = 5;
        numLevels = 5;
        timeIncrement = 0.5f;

        setUnlocalizedName(BlockInfo.TOTEM_INVISIBLE_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_INVISIBLE_TEXTURE);
    }
}
