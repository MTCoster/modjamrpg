package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreDirt extends TotemCore {
    public TotemCoreDirt(int id, Material material) {
        super(id, material, true);

        structure = new Structure[] {
                new Structure( 0, -1, -1, "stairWood"),
                new Structure( 0, -1,  1, "stairWood"),
                new Structure(-1, -1,  0, "stairWood"),
                new Structure( 0, -1,  0, "plankWood"),
                new Structure( 0,  0,  1, Block.cobblestoneWall.blockID),
                new Structure( 0,  0, -1, Block.cobblestoneWall.blockID),
                new Structure(-1,  0,  0, Block.cobblestoneWall.blockID),
                new Structure( 0,  1,  1, Block.dirt.blockID),
                new Structure( 0,  1, -1, Block.dirt.blockID),
                new Structure(-1,  1,  0, Block.gravel.blockID)
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.chickenCooked, 24),
                new ItemStack(Item.beefCooked, 24),
                new ItemStack(Item.porkCooked, 24),
                new ItemStack(Item.fishCooked, 24),
                new ItemStack(Item.chickenRaw, 32),
                new ItemStack(Item.beefRaw, 32),
                new ItemStack(Item.porkRaw, 32),
                new ItemStack(Item.fishRaw, 32),
        };

        talisman = Items.talismanDirt;

        potionID = 3;
        potionTime = 12000;
        levelsTaken = 5;
        numLevels = 5;

        setUnlocalizedName(BlockInfo.TOTEM_DIRT_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_DIRT_TEXTURE);
    }


}
