package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

import java.util.ArrayList;

public class TotemCoreRegen extends TotemCore {
    public TotemCoreRegen(int id, Material material) {
        super(id, material, true);

        structure = new Structure[]{
                new Structure(-1, -1, -1, Block.blockGold.blockID),
                new Structure(-1, -1,  1, Block.blockGold.blockID),
                new Structure(-1,  0, -1, "logWood"),
                new Structure( 0, -1,  0, "logWood"),
                new Structure(-1,  0,  1, "logWood"),
                new Structure(-1,  1, -1, "treeLeaves"),
                new Structure( 0,  1,  0, "treeLeaves"),
                new Structure(-1,  1,  1, "treeLeaves"),
                new Structure(-1, -1,  0, "treeLeaves")
        };

        ArrayList<ItemStack> items = OreDictionary.getOres("treeSapling");

        for (ItemStack item : items) {
            item.stackSize = 64;
        }

        activationItems = items.toArray(new ItemStack[items.size()]);

        talisman = Items.talismanRegen;

        potionID = 10;
        potionTime = 12000;
        levelsTaken = 5;
        numLevels = 5;
        timeIncrement = 0.0f;

        setUnlocalizedName(BlockInfo.TOTEM_REGEN_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_REGEN_TEXTURE);
    }
}
