package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreWither extends TotemCore {
    public TotemCoreWither(int id, Material material) {
        super(id, material, true);

        structure = new Structure[]{
                new Structure( 0, -2,  0, Block.netherBrick.blockID),
                new Structure( 0, -1,  0, Block.netherBrick.blockID),
                new Structure( 0, -1, -1, Block.slowSand.blockID),
                new Structure( 0, -1,  1, Block.slowSand.blockID),
                new Structure( 0,  0, -1, Block.stairsNetherBrick.blockID),
                new Structure( 0,  0,  1, Block.stairsNetherBrick.blockID),
                new Structure( 0,  1,  0, Block.stairsNetherBrick.blockID),
                new Structure( 0,  0,  1, Block.stairsNetherBrick.blockID),
                new Structure(-1, -1, -1, Block.skull.blockID),
                new Structure(-1, -1,  1, Block.skull.blockID)
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.netherStar, 1)
        };

        talisman = Items.talismanWither;

        potionID = 21;
        potionTime = 12000;
        levelsTaken = 10;
        numLevels = 10;
        timeIncrement = 0.20f;

        setUnlocalizedName(BlockInfo.TOTEM_WITHER_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_WITHER_TEXTURE);
    }
}
