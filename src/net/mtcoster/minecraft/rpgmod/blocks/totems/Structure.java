package net.mtcoster.minecraft.rpgmod.blocks.totems;

public class Structure {
    public static final int X = 0;  // +x
    public static final int Z = 1;  // +z
    public static final int XX = 2; // -x
    public static final int ZZ = 3; // -z
    public static final int maps[][] = {{X, Z}, {Z, XX}, {XX, ZZ}, {ZZ, X}};

    public static final int EAST = 0;
    public static final int SOUTH = 1;
    public static final int WEST = 2;
    public static final int NORTH = 3;

    public int x;
    public int y;
    public int z;
    public int blockID;
    public String oreDict = "";

    public Structure(int x, int y, int z, int blockID) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.blockID = blockID;
    }

    public Structure(int x, int y, int z, String oreDict) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.oreDict = oreDict;
    }

    public boolean isOreDict() {
        return !oreDict.equals("");
    }

    public Integer getMappedValue(int i) {
        switch (i) {
            case X:
                return x;
            case Z:
                return z;
            case XX:
                return 0 - x;
            case ZZ:
                return 0 - z;
        }

        return null;
    }
}
