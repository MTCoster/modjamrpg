package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreNight extends TotemCore {
    public TotemCoreNight(int id, Material material) {
        super(id, material, true);

        structure = new Structure[] {
                new Structure( 1, -1,  0, Block.blockNetherQuartz.blockID),
                new Structure( 1,  0,  0, Block.blockNetherQuartz.blockID),
                new Structure( 0, -1, -1, Block.stairsNetherQuartz.blockID),
                new Structure( 0, -1,  1, Block.stairsNetherQuartz.blockID),
                new Structure( 1, -1, -1, Block.blockRedstone.blockID),
                new Structure( 0, -1,  0, Block.blockRedstone.blockID),
                new Structure( 1, -1,  1, Block.blockRedstone.blockID),
                new Structure( 1,  0, -1, Block.redstoneLampActive.blockID),
                new Structure( 1,  0,  1, Block.redstoneLampActive.blockID),
                new Structure( 1,  1,  0, Block.glowStone.blockID),
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.goldenCarrot, 16)
        };

        talisman = Items.talismanNight;

        potionID = 16;
        potionTime = 6000;
        levelsTaken = 5;
        numLevels = 4;


        setUnlocalizedName(BlockInfo.TOTEM_NIGHT_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_NIGHT_TEXTURE);
    }
}
