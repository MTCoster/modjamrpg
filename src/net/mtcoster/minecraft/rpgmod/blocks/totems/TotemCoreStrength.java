package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreStrength extends TotemCore {
    public TotemCoreStrength(int id, Material material) {
        super(id, material, true);

        structure = new Structure[]{
                new Structure( 0, -1, -1, Block.blockIron.blockID),
                new Structure( 0, -1,  0, Block.blockIron.blockID),
                new Structure( 0, -1,  1, Block.blockIron.blockID),
                new Structure(-1, -1, -1, Block.reed.blockID),
                new Structure(-1,  0, -1, Block.reed.blockID),
                new Structure(-1,  0,  1, Block.reed.blockID),
                new Structure(-1, -1,  1, Block.reed.blockID),
                new Structure( 0,  1,  0, Block.cake.blockID),
                new Structure( 0,  0, -1, BlockInfo.COMPRESSED_REEDS_ID),
                new Structure( 0,  1, -1, BlockInfo.COMPRESSED_REEDS_ID),
                new Structure( 0,  1,  1, BlockInfo.COMPRESSED_REEDS_ID),
                new Structure( 0,  0,  1, BlockInfo.COMPRESSED_REEDS_ID)
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.swordDiamond, 1)
        };

        talisman = Items.talismanStrength;

        potionID = 5;
        potionTime = 3000;
        levelsTaken = 5;
        numLevels = 5;
        powerModifyer = 0.5f;

        setUnlocalizedName(BlockInfo.TOTEM_STRENGTH_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_STRENGTH_TEXTURE);
    }
}
