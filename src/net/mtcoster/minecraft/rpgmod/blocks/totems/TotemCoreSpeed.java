package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreSpeed extends TotemCore {
    public TotemCoreSpeed(int id, Material material) {
        super(id, material, true);

        structure = new Structure[]{
                new Structure( 0, -1, -1, Block.glowStone.blockID),
                new Structure( 0, -1,  0, Block.glowStone.blockID),
                new Structure( 0, -1,  1, Block.glowStone.blockID),
                new Structure(-1, -1, -1, Block.reed.blockID),
                new Structure(-1,  0, -1, Block.reed.blockID),
                new Structure(-1,  0,  1, Block.reed.blockID),
                new Structure(-1, -1,  1, Block.reed.blockID),
                new Structure( 0,  1,  0, Block.cake.blockID),
                new Structure( 0,  0, -1, BlockInfo.COMPRESSED_REEDS_ID),
                new Structure( 0,  1, -1, BlockInfo.COMPRESSED_REEDS_ID),
                new Structure( 0,  1,  1, BlockInfo.COMPRESSED_REEDS_ID),
                new Structure( 0,  0,  1, BlockInfo.COMPRESSED_REEDS_ID)
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.sugar, 64)
        };

        talisman = Items.talismanSpeed;

        potionID = 1;
        potionTime = 12000;
        levelsTaken = 5;
        numLevels = 5;

        setUnlocalizedName(BlockInfo.TOTEM_SPEED_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_SPEED_TEXTURE);
    }
}
