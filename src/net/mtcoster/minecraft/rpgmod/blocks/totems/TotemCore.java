package net.mtcoster.minecraft.rpgmod.blocks.totems;

import net.minecraft.block.BlockBreakable;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;
import net.mtcoster.minecraft.rpgmod.RpgMod;
import net.mtcoster.minecraft.rpgmod.items.Items;

import java.util.ArrayList;

public abstract class TotemCore extends BlockBreakable {
    Structure structure[];
    ItemStack activationItems[];
    int direction;
    Item talisman;

    int potionID = 0;
    int potionTime = 600;
    int levelsTaken = 5;
    int numLevels = 15;
    float levelIncrement = 0.25f;
    float timeIncrement = 0.20F;
    float powerModifyer = 1;

    public TotemCore(int id, Material material, boolean flag) {
        super(id, "", material, flag);
        setHardness(2.0F);
        setResistance(10.0F);
        setStepSound(soundStoneFootstep);
        setCreativeTab(RpgMod.instance.tabRpgMod);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float sideX, float sideY, float sideZ) {
        int meta = world.getBlockMetadata(x, y, z);
        return processClick(world, x, y, z, player, direction, meta);
    }

    @Override
    public int damageDropped(int meta) {
        return meta;
    }

    public int isCreated(World world, int x, int y, int z) {
        for (int i = 0; i < 4; i++) {
            boolean isValid = true;

            for (Structure block : structure) {
                int worldBlockID = world.getBlockId(x + block.getMappedValue(Structure.maps[i][Structure.X]), y + block.y, z + block.getMappedValue(Structure.maps[i][Structure.Z]));

                if (block.isOreDict()) {
                    ArrayList<ItemStack> oreDictBlocks = OreDictionary.getOres(block.oreDict);

                    boolean isInOreDict = false;

                    for (ItemStack itemStack : oreDictBlocks) {
                        if (itemStack.itemID != block.blockID) {
                            isInOreDict = true;
                            break;
                        }
                    }

                    if (!isInOreDict) {
                        isValid = false;
                        break;
                    }

                } else {
                    if (worldBlockID != block.blockID) {
                        isValid = false;
                        break;
                    }
                }
            }

            if (isValid) {
                return i;
            }
        }

        return -1;
    }

    public boolean processClick(World world, int x, int y, int z, EntityPlayer player, int direction, int meta) {
        ItemStack heldItem = player.getHeldItem();

        direction = isCreated(world, x, y, z);

        boolean giveEffect = true;

        if (direction < 0) {
            return false;
        }

        if (heldItem != null) {
            int splitSize = 0;

            for (ItemStack item : activationItems) {
                if (heldItem.itemID == item.itemID) {
                    // If held item is activator
                    if (heldItem.stackSize >= item.stackSize) {
                        int level = meta + 1;

                        if (level < numLevels) {
                            world.setBlockMetadataWithNotify(x, y, z, level, 3);
                            splitSize = item.stackSize;
                        } else {
                            return false;
                        }

                        giveEffect = false;
                    } else {
                        // If not enough of activation item in hand
                        return false;
                    }
                }
            }

            if (splitSize <= 0) {
                if (heldItem.itemID == Items.talismanInert.itemID) {
                    // If holding inert talisman
                    clearStructure(structure, direction, world, x, y, z);
                    generateTalisman(talisman, world, x, y, z, player, meta);
                    splitSize = 1;
                    giveEffect = false;
                }
            }

            player.inventoryContainer.getSlot(player.inventory.currentItem + 36).getStack().splitStack(splitSize);
        }

        // If hand is empty

        if (giveEffect) {
            giveEffect(world, x, y, z, player);
        }

        return true;
    }

    public void clearStructure(Structure[] structure, int direction, World world, int x, int y, int z) {
        for (Structure block : structure) {
            world.setBlockToAir(x + block.getMappedValue(Structure.maps[direction][Structure.X]), y + block.y, z + block.getMappedValue(Structure.maps[direction][Structure.Z]));
        }

        world.setBlockToAir(x, y, z);
    }

    public void generateTalisman(Item talisman, World world, int x, int y, int z, EntityPlayer player, int meta) {
        ItemStack droppedTalisman = new ItemStack(talisman, 1);
        droppedTalisman.setItemDamage(meta);

        EntityItem droppedItem = new EntityItem(world, x + 0.5F, y + 0.5F, z + 0.5F, droppedTalisman);

        droppedItem.motionX = 0.0F;
        droppedItem.motionY = 0.0F;
        droppedItem.motionZ = 0.0F;

        world.spawnEntityInWorld(droppedItem);
    }

    public void giveEffect(World world, int x, int y, int z, EntityPlayer player) {
        int power = world.getBlockMetadata(x, y, z);

        if(takeLevels(world, x, y, z, player)){
            if (power > 0) {
                player.addPotionEffect(new PotionEffect(potionID, (int)(potionTime + (potionTime * timeIncrement * (power - 1))), Math.round(powerModifyer *  (power - 1))));
            }
        }
    }

    public boolean takeLevels(World world, int x, int y, int z, EntityPlayer player){
        int level = player.experienceLevel;
        int power = world.getBlockMetadata(x,y,z);
        int lt = levelsTaken + (int)(levelsTaken * levelIncrement * power);

        if(power != 0){
            if (level >= lt || player.capabilities.isCreativeMode) {
                player.experienceLevel -= lt;
                return true;
            }
        }
        return false;

    }
}
