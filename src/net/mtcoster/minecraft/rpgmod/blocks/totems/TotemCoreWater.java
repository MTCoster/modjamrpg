package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreWater extends TotemCore {
    public TotemCoreWater(int id, Material material) {
        super(id, material, false);

        structure = new Structure[]{
                new Structure( 0,  0, -1, Block.ice.blockID),
                new Structure( 0,  1, -1, Block.ice.blockID),
                new Structure( 0,  1,  1, Block.ice.blockID),
                new Structure( 0,  0,  1, Block.ice.blockID),
                new Structure(-1, -1,  0, Block.waterlily.blockID),
                new Structure( 0, -1, -1, Block.waterStill.blockID),
                new Structure( 0, -1,  1, Block.waterStill.blockID)
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.dyePowder, 24, 0)
        };

        talisman = Items.talismanWater;

        potionID = 13;
        potionTime = 24000;
        levelsTaken = 5;
        numLevels = 5;

        setUnlocalizedName(BlockInfo.TOTEM_WATER_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_WATER_TEXTURE);
    }

    @Override
    public int getRenderBlockPass() {
        return 1;
    }
}
