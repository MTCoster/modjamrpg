package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

import java.util.Random;

public class TotemCoreEnder extends TotemCore {
    public TotemCoreEnder(int id, Material material) {
        super(id, material, true);

        structure = new Structure[] {
                new Structure(-1, -1, -1, Block.whiteStone.blockID),
                new Structure( 0,  1, -1, Block.whiteStone.blockID),
                new Structure( 1,  1,  0, Block.whiteStone.blockID),
                new Structure( 1,  2,  0, Block.whiteStone.blockID),
                new Structure( 0,  1,  1, Block.whiteStone.blockID),
                new Structure(-1, -1,  1, Block.whiteStone.blockID),
                new Structure( 0, -1,  0, Block.obsidian.blockID),
                new Structure(-1,  0, -1, Block.enderChest.blockID),
                new Structure(-1,  0,  1, Block.enderChest.blockID),
                new Structure( 0,  1,  0, Block.dragonEgg.blockID),
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.eyeOfEnder, 16)
        };

        talisman = Items.talismanEnder;

        potionID = 22;
        potionTime = 9000;
        levelsTaken = 10;
        numLevels = 10;

        setUnlocalizedName(BlockInfo.TOTEM_ENDER_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_ENDER_TEXTURE);
    }

    @Override
    public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random) {
        double d0 = (double)((float)par2 + par5Random.nextFloat());
        double d1 = (double)((float)par3 + 0.8F);
        double d2 = (double)((float)par4 + par5Random.nextFloat());
        double d3 = 0.0D;
        double d4 = 0.0D;
        double d5 = 0.0D;
        par1World.spawnParticle("smoke", d0, d1, d2, d3, d4, d5);
    }
}
