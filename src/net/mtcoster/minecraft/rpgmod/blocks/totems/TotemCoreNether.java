package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreNether extends TotemCore {
    public TotemCoreNether(int id, Material material) {
        super(id, material,true);

        structure = new Structure[] {
                new Structure( 0, -1,  0, Block.obsidian.blockID),
                new Structure( 1, -1,  0, Block.stairsNetherBrick.blockID),
                new Structure( 0, -1, -1, Block.stairsNetherBrick.blockID),
                new Structure( 0, -1,  1, Block.stairsNetherBrick.blockID),
                new Structure( 1,  0,  0, Block.netherrack.blockID),
                new Structure( 1,  1,  0, Block.fire.blockID),
                new Structure( 0,  0, -1, Block.lavaStill.blockID),
                new Structure( 0,  0,  1, Block.lavaStill.blockID),
                new Structure(-1,  0, -1, Block.fenceIron.blockID),
                new Structure(-1,  0, -2, Block.fenceIron.blockID),
                new Structure( 0,  0, -2, Block.fenceIron.blockID),
                new Structure( 1,  0, -2, Block.fenceIron.blockID),
                new Structure( 1,  0, -1, Block.fenceIron.blockID),
                new Structure( 1,  0,  1, Block.fenceIron.blockID),
                new Structure( 1,  0,  2, Block.fenceIron.blockID),
                new Structure( 0,  0,  2, Block.fenceIron.blockID),
                new Structure(-1,  0,  2, Block.fenceIron.blockID),
                new Structure(-1,  0,  1, Block.fenceIron.blockID),
                new Structure(-1, -1, -2, Block.netherFence.blockID),
                new Structure( 1, -1, -2, Block.netherFence.blockID),
                new Structure( 1, -1,  2, Block.netherFence.blockID),
                new Structure(-1, -1,  2, Block.netherFence.blockID),
                new Structure( 1,  1, -1, Block.netherFence.blockID),
                new Structure( 1,  2, -1, Block.netherFence.blockID),
                new Structure( 1,  2,  0, Block.netherFence.blockID),
                new Structure( 1,  2,  1, Block.netherFence.blockID),
                new Structure( 1,  1,  1, Block.netherFence.blockID)
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.netherStalkSeeds, 32)
        };

        talisman = Items.talismanNether;

        potionID = 12;
        potionTime = 12000;
        levelsTaken = 5;
        numLevels = 5;
        timeIncrement = 0.3F;

        setUnlocalizedName(BlockInfo.TOTEM_NETHER_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_NETHER_TEXTURE);
    }
}
