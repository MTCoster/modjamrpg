package net.mtcoster.minecraft.rpgmod.blocks.totems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.blocks.BlockInfo;
import net.mtcoster.minecraft.rpgmod.items.Items;

public class TotemCoreResistance extends TotemCore {
    public TotemCoreResistance(int id, Material material) {
        super(id, material, true);

        structure = new Structure[]{
                new Structure( 1,  0, -1, Block.blockIron.blockID),
                new Structure( 1,  0,  0, Block.blockIron.blockID),
                new Structure( 1,  0,  1, Block.blockIron.blockID),
                new Structure( 0, -1, -1, BlockInfo.STAIRS_IRON_ID),
                new Structure( 1, -1, -1, BlockInfo.STAIRS_IRON_ID),
                new Structure( 1, -1,  0, BlockInfo.STAIRS_IRON_ID),
                new Structure( 1, -1,  1, BlockInfo.STAIRS_IRON_ID),
                new Structure( 1, -1,  1, BlockInfo.STAIRS_IRON_ID),
                new Structure( 0,  0, -1, BlockInfo.STAIRS_IRON_ID),
                new Structure( 1,  1, -1, BlockInfo.STAIRS_IRON_ID),
                new Structure( 1,  1,  1, BlockInfo.STAIRS_IRON_ID),
                new Structure( 0,  0,  1, BlockInfo.STAIRS_IRON_ID),
                new Structure( 0, -1,  0, BlockInfo.IRON_DIAMOND_ID),
                new Structure( 1,  1,  0, BlockInfo.IRON_DIAMOND_ID)
        };

        activationItems = new ItemStack[]{
                new ItemStack(Item.plateDiamond, 1)
        };

        talisman = Items.talismanResistance;

        potionID = 11;
        potionTime = 1800;
        levelsTaken = 5;
        numLevels = 4;

        setUnlocalizedName(BlockInfo.TOTEM_RESISTANCE_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        blockIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.TOTEM_RESISTANCE_TEXTURE);
    }
}
