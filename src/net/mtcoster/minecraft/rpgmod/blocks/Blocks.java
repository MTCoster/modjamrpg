package net.mtcoster.minecraft.rpgmod.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.mtcoster.minecraft.rpgmod.blocks.blocks.*;
import net.mtcoster.minecraft.rpgmod.blocks.totems.*;
import net.mtcoster.minecraft.rpgmod.items.totems.*;

public class Blocks {
    public static Block totemDirt;
    public static Block totemEnder;
    public static Block totemFarming;
    public static Block totemHeal;
    public static Block totemInvisible;
    public static Block totemNether;
    public static Block totemNight;
    public static Block totemRegen;
    public static Block totemResistance;
    public static Block totemSpeed;
    public static Block totemStrength;
    public static Block totemWater;
    public static Block totemWither;
    public static Block compressedReeds;
    public static Block appleCrate;
    public static Block ironDiamond;
    public static Block stairsCoal;
    public static Block stairsDiamond;
    public static Block stairsEmerald;
    public static Block stairsGold;
    public static Block stairsIron;
    public static Block stairsLapis;
    public static Block stairsRedstone;

    public static void initBlocks() {
        totemDirt = new TotemCoreDirt(BlockInfo.TOTEM_DIRT_ID, Material.rock);
        totemEnder = new TotemCoreEnder(BlockInfo.TOTEM_ENDER_ID, Material.rock);
        totemFarming = new TotemCoreFarming(BlockInfo.TOTEM_FARMING_ID, Material.rock);
        totemHeal = new TotemCoreHeal(BlockInfo.TOTEM_HEAL_ID, Material.rock);
        totemInvisible = new TotemCoreInvisible(BlockInfo.TOTEM_INVISIBLE_ID, Material.rock);
        totemNether = new TotemCoreNether(BlockInfo.TOTEM_NETHER_ID, Material.rock);
        totemNight = new TotemCoreNight(BlockInfo.TOTEM_NIGHT_ID, Material.rock);
        totemRegen = new TotemCoreRegen(BlockInfo.TOTEM_REGEN_ID, Material.rock);
        totemResistance = new TotemCoreResistance(BlockInfo.TOTEM_RESISTANCE_ID, Material.rock);
        totemSpeed = new TotemCoreSpeed(BlockInfo.TOTEM_SPEED_ID, Material.rock);
        totemStrength = new TotemCoreStrength(BlockInfo.TOTEM_STRENGTH_ID, Material.rock);
        totemWater = new TotemCoreWater(BlockInfo.TOTEM_WATER_ID, Material.rock);
        totemWither = new TotemCoreWither(BlockInfo.TOTEM_WITHER_ID, Material.rock);
        compressedReeds = new BlockCompressedReeds(BlockInfo.COMPRESSED_REEDS_ID);
        appleCrate = new BlockAppleCrate(BlockInfo.APPLE_CRATE_ID);
        ironDiamond = new BlockIronDiamond(BlockInfo.IRON_DIAMOND_ID);
        stairsCoal = new BlockCoalStairs(BlockInfo.STAIRS_COAL_ID);
        stairsDiamond = new BlockDiamondStairs(BlockInfo.STAIRS_DIAMOND_ID);
        stairsEmerald = new BlockEmeraldStairs(BlockInfo.STAIRS_EMERALD_ID);
        stairsGold = new BlockGoldStairs(BlockInfo.STAIRS_GOLD_ID);
        stairsIron = new BlockIronStairs(BlockInfo.STAIRS_IRON_ID);
        stairsLapis = new BlockLapisStairs(BlockInfo.STAIRS_LAPIS_ID);
        stairsRedstone = new BlockRedstoneStairs(BlockInfo.STAIRS_REDSTONE_ID);

        GameRegistry.registerBlock(totemDirt, ItemTotemCoreDirt.class, BlockInfo.TOTEM_DIRT_NAME);
        GameRegistry.registerBlock(totemEnder, ItemTotemCoreEnder.class, BlockInfo.TOTEM_ENDER_NAME);
        GameRegistry.registerBlock(totemFarming, ItemTotemCoreFarming.class, BlockInfo.TOTEM_FARMING_NAME);
        GameRegistry.registerBlock(totemHeal, ItemTotemCoreHeal.class, BlockInfo.TOTEM_HEAL_NAME);
        GameRegistry.registerBlock(totemInvisible, ItemTotemCoreInvisible.class, BlockInfo.TOTEM_INVISIBLE_NAME);
        GameRegistry.registerBlock(totemNether, ItemTotemCoreNether.class, BlockInfo.TOTEM_NETHER_NAME);
        GameRegistry.registerBlock(totemNight, ItemTotemCoreNight.class, BlockInfo.TOTEM_NIGHT_NAME);
        GameRegistry.registerBlock(totemRegen, ItemTotemCoreRegen.class, BlockInfo.TOTEM_REGEN_NAME);
        GameRegistry.registerBlock(totemResistance, ItemTotemCoreResistance.class, BlockInfo.TOTEM_RESISTANCE_NAME);
        GameRegistry.registerBlock(totemSpeed, ItemTotemCoreSpeed.class, BlockInfo.TOTEM_SPEED_NAME);
        GameRegistry.registerBlock(totemStrength, ItemTotemCoreStrength.class, BlockInfo.TOTEM_STRENGTH_NAME);
        GameRegistry.registerBlock(totemWater, ItemTotemCoreWater.class, BlockInfo.TOTEM_WATER_NAME);
        GameRegistry.registerBlock(totemWither, ItemTotemCoreWither.class, BlockInfo.TOTEM_WITHER_NAME);

        GameRegistry.registerBlock(compressedReeds, BlockInfo.COMPRESSED_REEDS_NAME);
        GameRegistry.registerBlock(appleCrate, BlockInfo.APPLE_CRATE_NAME);
        GameRegistry.registerBlock(ironDiamond, BlockInfo.IRON_DIAMOND_NAME);

        GameRegistry.registerBlock(stairsCoal, BlockInfo.STAIRS_COAL_NAME);
        GameRegistry.registerBlock(stairsDiamond, BlockInfo.STAIRS_DIAMOND_NAME);
        GameRegistry.registerBlock(stairsEmerald, BlockInfo.STAIRS_EMERALD_NAME);
        GameRegistry.registerBlock(stairsGold, BlockInfo.STAIRS_GOLD_NAME);
        GameRegistry.registerBlock(stairsIron, BlockInfo.STAIRS_IRON_NAME);
        GameRegistry.registerBlock(stairsLapis, BlockInfo.STAIRS_LAPIS_NAME);
        GameRegistry.registerBlock(stairsRedstone, BlockInfo.STAIRS_REDSTONE_NAME);
    }

    public static void registerCrafting() {
        GameRegistry.addRecipe(new ShapedOreRecipe(totemDirt, "cwc", "dgd", "cwc", 'c', Block.cobblestone, 'd', Block.dirt, 'g', Block.gravel, 'w', "plankWood"));
        GameRegistry.addShapedRecipe(new ItemStack(totemEnder, 1), "ses", "eoe", "ses", 's', Block.whiteStone, 'e', Item.eyeOfEnder, 'o', Block.obsidian);
        GameRegistry.addShapedRecipe(new ItemStack(totemFarming, 1), "hdh", "cpw", "hdh", 'h', Block.field_111038_cB, 'd', Block.dirt, 'c', Item.carrot, 'p', Item.potato, 'w', Item.wheat);
        GameRegistry.addShapedRecipe(new ItemStack(totemHeal, 1), "mgm", "pap", "mpm", 'm', Block.melon, 'p', Block.pumpkin, 'a', Item.appleRed, 'g', Item.ingotGold);
        GameRegistry.addShapedRecipe(new ItemStack(totemInvisible, 1), "eoe", "oto", "eoe", 'e', Item.enderPearl, 'o', Block.obsidian, 't', Block.enchantmentTable);
        GameRegistry.addShapedRecipe(new ItemStack(totemNether, 1), "bcb", "sgs", "brb", 'b', Block.netherBrick, 'c', Item.fireballCharge, 'r', Block.netherrack, 's', Block.slowSand, 'g', Item.ghastTear);
        GameRegistry.addShapedRecipe(new ItemStack(totemNight, 1), "gcg", "qrq", "gqg", 'g', Block.glowStone,'c', Item.goldenCarrot, 'q', Block.blockNetherQuartz, 'r', Block.blockRedstone);
        GameRegistry.addShapedRecipe(new ItemStack(totemRegen, 1), "lgl", "awa", "lgl", 'g', Block.blockGold, 'l', Block.leaves, 'a', Item.appleRed, 'w', Block.wood);
        GameRegistry.addShapedRecipe(new ItemStack(totemResistance), "oio", "dod", "oio", 'o', Block.obsidian, 'i', Block.blockIron, 'd', Item.diamond);
        GameRegistry.addShapedRecipe(new ItemStack(totemSpeed, 1), "grg", "rcr", "grg", 'g', Block.glowStone, 'r', Blocks.compressedReeds, 'c', Block.cake);
        GameRegistry.addShapedRecipe(new ItemStack(totemStrength,1), "rcr", "isi", "ror", 'r', Blocks.compressedReeds, 'c', Block.cake, 'i', Block.blockIron, 's', Item.swordIron, 'o', Block.obsidian);
        GameRegistry.addShapedRecipe(new ItemStack(totemWater,1), "iwi", "wsw", "iwi", 'i', Block.ice, 'w', Item.bucketWater, 's', new ItemStack(Item.dyePowder, 1, 0));
        GameRegistry.addShapedRecipe(new ItemStack(totemWither, 1), "hsh", "sbs", "hsh", 'h', new ItemStack(Item.skull, 1, 1), 's', Block.slowSand, 'b', Block.netherBrick);

        GameRegistry.addShapelessRecipe(new ItemStack(ironDiamond, 1), Block.blockIron, Item.diamond);

        GameRegistry.addShapedRecipe(new ItemStack(stairsCoal, 4), "i  ", "ii ", "iii", 'i', Item.coal);
        GameRegistry.addShapedRecipe(new ItemStack(stairsCoal, 4), "  i", " ii", "iii", 'i', Item.coal);
        GameRegistry.addShapedRecipe(new ItemStack(stairsDiamond, 4), "i  ", "ii ", "iii", 'i', Item.diamond);
        GameRegistry.addShapedRecipe(new ItemStack(stairsDiamond, 4), "  i", " ii", "iii", 'i', Item.diamond);
        GameRegistry.addShapedRecipe(new ItemStack(stairsEmerald, 4), "i  ", "ii ", "iii", 'i', Item.emerald);
        GameRegistry.addShapedRecipe(new ItemStack(stairsEmerald, 4), "  i", " ii", "iii", 'i', Item.emerald);
        GameRegistry.addShapedRecipe(new ItemStack(stairsGold, 4), "i  ", "ii ", "iii", 'i', Item.ingotGold);
        GameRegistry.addShapedRecipe(new ItemStack(stairsGold, 4), "  i", " ii", "iii", 'i', Item.ingotGold);
        GameRegistry.addShapedRecipe(new ItemStack(stairsIron, 4), "i  ", "ii ", "iii", 'i', Item.ingotIron);
        GameRegistry.addShapedRecipe(new ItemStack(stairsIron, 4), "  i", " ii", "iii", 'i', Item.ingotIron);
        GameRegistry.addShapedRecipe(new ItemStack(stairsLapis, 4), "i  ", "ii ", "iii", 'i', new ItemStack(Item.dyePowder, 1, 4));
        GameRegistry.addShapedRecipe(new ItemStack(stairsLapis, 4), "  i", " ii", "iii", 'i', new ItemStack(Item.dyePowder, 1, 4));
        GameRegistry.addShapedRecipe(new ItemStack(stairsRedstone, 4), "i  ", "ii ", "iii", 'i', Item.redstone);
        GameRegistry.addShapedRecipe(new ItemStack(stairsRedstone, 4), "  i", " ii", "iii", 'i', Item.redstone);
    }

    public static void registerLang() {
        LanguageRegistry.addName(totemDirt, BlockInfo.TOTEM_DIRT_NAME);
        LanguageRegistry.addName(totemEnder, BlockInfo.TOTEM_ENDER_NAME);
        LanguageRegistry.addName(totemFarming, BlockInfo.TOTEM_FARMING_NAME);
        LanguageRegistry.addName(totemHeal, BlockInfo.TOTEM_HEAL_NAME);
        LanguageRegistry.addName(totemInvisible, BlockInfo.TOTEM_INVISIBLE_NAME);
        LanguageRegistry.addName(totemNether, BlockInfo.TOTEM_NETHER_NAME);
        LanguageRegistry.addName(totemNight, BlockInfo.TOTEM_NIGHT_NAME);
        LanguageRegistry.addName(totemRegen, BlockInfo.TOTEM_REGEN_NAME);
        LanguageRegistry.addName(totemResistance, BlockInfo.TOTEM_RESISTANCE_NAME);
        LanguageRegistry.addName(totemSpeed, BlockInfo.TOTEM_SPEED_NAME);
        LanguageRegistry.addName(totemStrength, BlockInfo.TOTEM_STRENGTH_NAME);
        LanguageRegistry.addName(totemWater, BlockInfo.TOTEM_WATER_NAME);
        LanguageRegistry.addName(totemWither, BlockInfo.TOTEM_WITHER_NAME);
        LanguageRegistry.addName(compressedReeds, BlockInfo.COMPRESSED_REEDS_NAME);
        LanguageRegistry.addName(appleCrate, BlockInfo.APPLE_CRATE_NAME);
        LanguageRegistry.addName(ironDiamond, BlockInfo.IRON_DIAMOND_NAME);
        LanguageRegistry.addName(stairsCoal, BlockInfo.STAIRS_COAL_NAME);
        LanguageRegistry.addName(stairsDiamond, BlockInfo.STAIRS_DIAMOND_NAME);
        LanguageRegistry.addName(stairsEmerald, BlockInfo.STAIRS_EMERALD_NAME);
        LanguageRegistry.addName(stairsGold, BlockInfo.STAIRS_GOLD_NAME);
        LanguageRegistry.addName(stairsIron, BlockInfo.STAIRS_IRON_NAME);
        LanguageRegistry.addName(stairsLapis, BlockInfo.STAIRS_LAPIS_NAME);
        LanguageRegistry.addName(stairsRedstone, BlockInfo.STAIRS_REDSTONE_NAME);
    }
}
