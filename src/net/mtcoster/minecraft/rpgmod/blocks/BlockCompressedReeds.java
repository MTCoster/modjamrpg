package net.mtcoster.minecraft.rpgmod.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBreakable;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.Icon;
import net.mtcoster.minecraft.rpgmod.ModInfo;

public class BlockCompressedReeds extends BlockBreakable {
    public BlockCompressedReeds(int id) {
        super(id,ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.COMPRESSED_REEDS_SIDE_TEXTURE, Material.grass, false);
        setHardness(0.5f);
        setCreativeTab(CreativeTabs.tabDecorations);
        setStepSound(Block.soundGrassFootstep);
        setUnlocalizedName(BlockInfo.COMPRESSED_REEDS_UNLOCAL_NAME);
    }

    @SideOnly(Side.CLIENT)
    public Icon sideIcon;
    @SideOnly(Side.CLIENT)
    public Icon topIcon;

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        sideIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.COMPRESSED_REEDS_SIDE_TEXTURE);
        topIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + BlockInfo.COMPRESSED_REEDS_TOP_TEXTURE);
    }

    @Override
    public Icon getIcon(int side, int meta) {
        if(side == 0 || side == 1){
            return topIcon;
        }
        else{
            return sideIcon;
        }
    }

}
