package net.mtcoster.minecraft.rpgmod.items;


import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.mtcoster.minecraft.rpgmod.items.talismans.*;

public class Items {
    public static Item talismanInert;
    public static Item talismanDirt;
    public static Item talismanEnder;
    public static Item talismanFarming;
    public static Item talismanHeal;
    public static Item talismanInvisible;
    public static Item talismanNether;
    public static Item talismanNight;
    public static Item talismanRegen;
    public static Item talismanResistance;
    public static Item talismanSpeed;
    public static Item talismanStrength;
    public static Item talismanWater;
    public static Item talismanWither;

    public static void initItems() {
        talismanInert     = new TalismanInert(ItemInfo.TALISMAN_INERT_ID);
        talismanDirt      = new TalismanDirt(ItemInfo.TALISMAN_DIRT_ID);
        talismanEnder     = new TalismanEnder(ItemInfo.TALISMAN_ENDER_ID);
        talismanFarming   = new TalismanFarming(ItemInfo.TALISMAN_FARMING_ID);
        talismanHeal      = new TalismanHeal(ItemInfo.TALISMAN_HEAL_ID);
        talismanInvisible = new TalismanInvisible(ItemInfo.TALISMAN_INVISIBLE_ID);
        talismanNether    = new TalismanNether(ItemInfo.TALISMAN_NETHER_ID);
        talismanNight     = new TalismanNight(ItemInfo.TALISMAN_NIGHT_ID);
        talismanRegen     = new TalismanRegen(ItemInfo.TALISMAN_REGEN_ID);
        talismanResistance= new TalismanResistance(ItemInfo.TALISMAN_RESISTANCE_ID);
        talismanSpeed     = new TalismanSpeed(ItemInfo.TALISMAN_SPEED_ID);
        talismanStrength  = new TalismanStrength(ItemInfo.TALISMAN_STRENGTH_ID);
        talismanWater     = new TalismanWater(ItemInfo.TALISMAN_WATER_ID);
        talismanWither    = new TalismanWither(ItemInfo.TALISMAN_WITHER_ID);

        GameRegistry.registerItem(talismanInert, ItemInfo.TALISMAN_INERT_NAME);
        GameRegistry.registerItem(talismanDirt, ItemInfo.TALISMAN_DIRT_NAME);
        GameRegistry.registerItem(talismanEnder, ItemInfo.TALISMAN_ENDER_NAME);
        GameRegistry.registerItem(talismanFarming, ItemInfo.TALISMAN_FARMING_NAME);
        GameRegistry.registerItem(talismanHeal, ItemInfo.TALISMAN_HEAL_NAME);
        GameRegistry.registerItem(talismanInvisible, ItemInfo.TALISMAN_INVISIBLE_NAME);
        GameRegistry.registerItem(talismanNether, ItemInfo.TALISMAN_NETHER_NAME);
        GameRegistry.registerItem(talismanNight, ItemInfo.TALISMAN_NIGHT_NAME);
        GameRegistry.registerItem(talismanRegen, ItemInfo.TALISMAN_REGEN_NAME);
        GameRegistry.registerItem(talismanResistance, ItemInfo.TALISMAN_RESISTANCE_NAME);
        GameRegistry.registerItem(talismanSpeed, ItemInfo.TALISMAN_SPEED_NAME);
        GameRegistry.registerItem(talismanStrength, ItemInfo.TALISMAN_STRENGTH_NAME);
        GameRegistry.registerItem(talismanWater, ItemInfo.TALISMAN_WATER_NAME);
        GameRegistry.registerItem(talismanWither, ItemInfo.TALISMAN_WITHER_NAME);
    }

    public static void registerCrafting() {
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(talismanInert, 4), "wsw", "sss", "wsw", 'w', "plankWood", 's', Block.cobblestone));
    }

    public static void registerLang() {
        LanguageRegistry.addName(talismanInert, ItemInfo.TALISMAN_INERT_NAME);
        LanguageRegistry.addName(talismanDirt, ItemInfo.TALISMAN_DIRT_NAME);
        LanguageRegistry.addName(talismanEnder, ItemInfo.TALISMAN_ENDER_NAME);
        LanguageRegistry.addName(talismanFarming, ItemInfo.TALISMAN_FARMING_NAME);
        LanguageRegistry.addName(talismanHeal, ItemInfo.TALISMAN_HEAL_NAME);
        LanguageRegistry.addName(talismanInvisible, ItemInfo.TALISMAN_INVISIBLE_NAME);
        LanguageRegistry.addName(talismanNether, ItemInfo.TALISMAN_NETHER_NAME);
        LanguageRegistry.addName(talismanNight, ItemInfo.TALISMAN_NIGHT_NAME);
        LanguageRegistry.addName(talismanRegen, ItemInfo.TALISMAN_REGEN_NAME);
        LanguageRegistry.addName(talismanResistance, ItemInfo.TALISMAN_RESISTANCE_NAME);
        LanguageRegistry.addName(talismanSpeed, ItemInfo.TALISMAN_SPEED_NAME);
        LanguageRegistry.addName(talismanStrength, ItemInfo.TALISMAN_STRENGTH_NAME);
        LanguageRegistry.addName(talismanWater, ItemInfo.TALISMAN_WATER_NAME);
        LanguageRegistry.addName(talismanWither, ItemInfo.TALISMAN_WITHER_NAME);

    }

}
