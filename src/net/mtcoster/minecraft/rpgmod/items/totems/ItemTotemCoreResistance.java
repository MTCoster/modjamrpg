package net.mtcoster.minecraft.rpgmod.items.totems;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ItemTotemCoreResistance extends ItemTotemCore {
    public ItemTotemCoreResistance(int id) {
        super(id);
    }
    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4) {
        int level = getDamage(stack);
        list.add("Activated with 1 diamond chestplate");
        list.add("\u00a7bCurrently level \u00a7e" + level);
    }
}
