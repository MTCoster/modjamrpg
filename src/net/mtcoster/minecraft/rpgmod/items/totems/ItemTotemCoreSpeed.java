package net.mtcoster.minecraft.rpgmod.items.totems;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ItemTotemCoreSpeed extends ItemTotemCore {
    public ItemTotemCoreSpeed(int id) {
        super(id);
    }
    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4) {
        int level = getDamage(stack);
        list.add("Activated with 64 sugar");
        list.add("\u00a7bCurrently level \u00a7e" + level);
    }
}

