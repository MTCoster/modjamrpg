package net.mtcoster.minecraft.rpgmod.items.totems;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ItemTotemCoreFarming extends ItemTotemCore {
    public ItemTotemCoreFarming(int id) {
        super(id);
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4) {
        int level = getDamage(stack);
        list.add("Activated with 48 carrots, potatoes or seeds,");
        list.add("16 melon/pumpkin seeds or 322 wheat");
        list.add("\u00a7bCurrently level \u00a7e" + level);
    }
}
