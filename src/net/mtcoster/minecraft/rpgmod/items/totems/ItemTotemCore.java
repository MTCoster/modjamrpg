package net.mtcoster.minecraft.rpgmod.items.totems;

import net.minecraft.item.ItemBlock;

public abstract class ItemTotemCore extends ItemBlock{
    public ItemTotemCore(int id) {
        super(id);
        setHasSubtypes(true);
    }

    @Override
    public int getMetadata(int dmg) {
        return dmg;
    }
}
