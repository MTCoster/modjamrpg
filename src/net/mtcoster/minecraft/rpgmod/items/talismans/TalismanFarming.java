package net.mtcoster.minecraft.rpgmod.items.talismans;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.items.ItemInfo;

import java.util.List;


public class TalismanFarming extends Talisman{
    public TalismanFarming(int id) {
        super(id);
        setUnlocalizedName(ItemInfo.TALISMAN_FARMING_UNLOCAL_NAME);
        potionID = 23;
        potionTime = 1;
        levelsTaken = 5;
        timeIncrement = 0.5f;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        itemIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + ItemInfo.TALISMAN_FARMING_TEXTURE);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, EntityPlayer player, List info, boolean useExtraInformation) {

        int dmg = getDamage(stack);
        int time = Math.round((potionTime + (potionTime * timeIncrement * dmg))/20);
        info.add("\u00a7fInsta-food in case of hunger");
        info.add("\u00a7bRegens \u00a7e" + (dmg * 2) + "\u00a7b points of hunger");
        info.add("\u00a7bCosts \u00a7a" + (levelsTaken + (dmg - 1)) + " \u00a7blevels");
    }

    @Override
    public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float sidex, float sidey, float sidez) {
        int power = getDamage(stack);

        if (takeLevels(world, x, y, z, player, stack)){
            if(power > 0){
                player.addPotionEffect(new PotionEffect(potionID, 2 * power, 1));
                return true;
            }
        }
        return false;
    }
}

