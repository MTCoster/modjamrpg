package net.mtcoster.minecraft.rpgmod.items.talismans;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.items.ItemInfo;

import java.util.List;

public class TalismanResistance extends Talisman {
    public TalismanResistance(int id) {
        super(id);
        setUnlocalizedName(ItemInfo.TALISMAN_RESISTANCE_UNLOCAL_NAME);
        potionID = 11;
        potionTime = 600;
        levelsTaken = 4;
        timeIncrement = 0.2f;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        itemIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + ItemInfo.TALISMAN_RESISTANCE_TEXTURE);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, EntityPlayer player, List info, boolean useExtraInformation) {
        String name = Talisman.effectNames[potionID];
        int dmg = getDamage(stack);
        int time = Math.round((potionTime + (potionTime * timeIncrement * (dmg - 1)))/20);
        info.add("\u00a7fThis should help with the hurting...");
        info.add("\u00a7bGives \u00a7e" + name + "\u00a7b level \u00a7e" + (dmg + 1));
        info.add("\u00a7bfor \u00a7e" + time + "§b seconds");
        info.add("\u00a7bCosts \u00a7a" + (levelsTaken + (dmg - 1)) + " \u00a7blevels");
    }
}
