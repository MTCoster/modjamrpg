package net.mtcoster.minecraft.rpgmod.items.talismans;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.items.ItemInfo;

import java.util.List;

public class TalismanHeal extends Talisman {
    public TalismanHeal(int id) {
        super(id);
        setUnlocalizedName(ItemInfo.TALISMAN_HEAL_UNLOCAL_NAME);
        potionID = 6;
        potionTime = 5;
        levelsTaken = 5;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        itemIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + ItemInfo.TALISMAN_HEAL_TEXTURE);
    }

    @Override
    public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float sidex, float sidey, float sidez) {
        int power = getDamage(stack);

        if (takeLevels(world, x, y, z, player, stack)){
            if(power > 0){
                player.addPotionEffect(new PotionEffect(potionID, 1, power));
                return true;
            }
        }
        return false;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, EntityPlayer player, List info, boolean useExtraInformation) {

        int dmg = getDamage(stack);
        int time = Math.round((potionTime + (potionTime * timeIncrement * (dmg - 1)))/20);
        info.add("\u00a7fInsta-health in case of near-death experiences");
        info.add("\u00a7bRegens \u00a7e" + (dmg * 4) + "\u00a7b points of health");
        info.add("\u00a7bCosts \u00a7a" + (levelsTaken + (dmg - 1)) + " \u00a7blevels");
    }

}
