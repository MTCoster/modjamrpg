package net.mtcoster.minecraft.rpgmod.items.talismans;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.items.ItemInfo;

import java.util.List;

public class TalismanInvisible extends Talisman {
    public TalismanInvisible(int id) {
        super(id);
        setUnlocalizedName(ItemInfo.TALISMAN_INVISIBLE_UNLOCAL_NAME);
        potionID = 14;
        potionTime = 2400;
        levelsTaken = 5;
        timeIncrement = 0.5f;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        itemIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + ItemInfo.TALISMAN_INVISIBLE_TEXTURE);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, EntityPlayer player, List info, boolean useExtraInformation) {
        String name = Talisman.effectNames[potionID];
        int dmg = getDamage(stack);
        int time = Math.round(potionTime + (potionTime * timeIncrement * (dmg - 1)))/20;
        info.add("\u00a7fHide from your enemies...");
        info.add("\u00a7bGives \u00a7e" + name);
        info.add("\u00a7bfor \u00a7e" + time + "\u00a7b seconds");
        info.add("\u00a7bCosts \u00a7a" + (levelsTaken + (dmg - 1)) + " \u00a7blevels");
    }
}
