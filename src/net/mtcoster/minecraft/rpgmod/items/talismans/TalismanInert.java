package net.mtcoster.minecraft.rpgmod.items.talismans;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.items.ItemInfo;

import java.util.List;

public class TalismanInert extends Talisman {
    public TalismanInert(int id) {
        super(id);
        setUnlocalizedName(ItemInfo.TALISMAN_INERT_UNLOCAL_NAME);
        setMaxStackSize(16);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        itemIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + ItemInfo.TALISMAN_INERT_TEXTURE);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, EntityPlayer player, List info, boolean useExtraInformation) {
        String name = Talisman.effectNames[potionID];
        int dmg = getDamage(stack);
        int time = Math.round((potionTime * timeIncrement * dmg)/20);
        info.add("A usless round stone");
        info.add("Rigth click on a leveled up totem to activate");
    }
}
