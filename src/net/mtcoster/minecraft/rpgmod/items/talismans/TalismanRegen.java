package net.mtcoster.minecraft.rpgmod.items.talismans;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.mtcoster.minecraft.rpgmod.ModInfo;
import net.mtcoster.minecraft.rpgmod.items.ItemInfo;

import java.util.List;

public class TalismanRegen extends Talisman {
    public TalismanRegen(int id) {
        super(id);
        setUnlocalizedName(ItemInfo.TALISMAN_REGEN_UNLOCAL_NAME);
        potionID = 10;
        potionTime = 100;
        levelsTaken = 3;
        timeIncrement = 0.0f;

    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerIcons(IconRegister register) {
        itemIcon = register.registerIcon(ModInfo.TEXTURE_LOCATION + ":" + ItemInfo.TALISMAN_REGEN_TEXTURE);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, EntityPlayer player, List info, boolean useExtraInformation) {
        String name = Talisman.effectNames[potionID];
        int dmg = getDamage(stack);
        int time = Math.round(potionTime + (potionTime * timeIncrement * (dmg - 1)))/20;
        float regenTime = (float)(50 * (1 / Math.pow(2, dmg-1)))/20;
        info.add("\u00a7fA little pick-me-up...");
        info.add("\u00a7bRegens \u00a7e1\u00a7b point of health every \u00a7e" + regenTime);
        info.add("\u00a7bfor \u00a7e" + time + "\u00a7b seconds");
        info.add("\u00a7bCosts \u00a7a" + (levelsTaken + (dmg - 1)) + " \u00a7blevels");
    }
}
