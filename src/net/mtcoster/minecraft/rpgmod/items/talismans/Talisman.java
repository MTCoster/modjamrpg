package net.mtcoster.minecraft.rpgmod.items.talismans;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.mtcoster.minecraft.rpgmod.RpgMod;

public abstract class Talisman extends Item {
    int potionID;
    int potionTime = 600;
    int levelsTaken = 5;
    float levelIncrement = 0.25f;
    float timeIncrement = 0.20F;
    float powerModifier = 1;

    static String[] effectNames;

    static{
        effectNames = new String[]{
                "",
                "Speed",
                "Slowness",
                "Haste",
                "Mining Fatigue",
                "Strength",
                "Instant Health",
                "Instant Damage",
                "Jump Boost",
                "Nausea",
                "Regeneration",
                "Resistance",
                "Fire Resistance",
                "Water Breathing",
                "Invisibility",
                "Blindness",
                "Night Vision",
                "Hunger",
                "Weakness",
                "Poison",
                "Wither",
                "Health Boost",
                "Absorption"
        };
    }

    public Talisman(int id) {
        super(id);
        setCreativeTab(RpgMod.instance.tabRpgMod);
        setMaxStackSize(1);
    }

    @Override
    public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float sidex, float sidey, float sidez) {
        int power = getDamage(stack) - 1;

        if (takeLevels(world, x, y, z, player, stack)){
            if(power >= 0){
                player.addPotionEffect(new PotionEffect(potionID, Math.round(potionTime + (potionTime * timeIncrement * power)), Math.round(powerModifier * power)));
                return true;
            }
        }
        return false;
    }

    public boolean takeLevels(World world, int x, int y, int z, EntityPlayer player, ItemStack stack){
        int level = player.experienceLevel;
        int power = getDamage(stack) - 1;
        int lt = levelsTaken + power;

        if (level >= lt || player.capabilities.isCreativeMode) {
            player.experienceLevel -= lt;
            return true;
        }
        return false;
    }


}
