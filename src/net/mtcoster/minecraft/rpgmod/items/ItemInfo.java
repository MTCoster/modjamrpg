package net.mtcoster.minecraft.rpgmod.items;

public class ItemInfo {

    public static int TALISMAN_DIRT_ID;
    public static final int TALISMAN_DIRT_DEFAULT_ID = 4401;
    public static final String TALISMAN_DIRT_TEXTURE = "talisman_dirt";
    public static final String TALISMAN_DIRT_UNLOCAL_NAME = "DirtTalisman";
    public static final String TALISMAN_DIRT_NAME = "\u00a7dTalisman of the Earth";

    public static int TALISMAN_ENDER_ID;
    public static final int TALISMAN_ENDER_DEFAULT_ID = 4402;
    public static final String TALISMAN_ENDER_TEXTURE = "talisman_ender";
    public static final String TALISMAN_ENDER_UNLOCAL_NAME = "EnderTalisman";
    public static final String TALISMAN_ENDER_NAME = "\u00a7dTalisman of the End";

    public static int TALISMAN_FARMING_ID;
    public static final int TALISMAN_FARMING_DEFAULT_ID = 4403;
    public static final String TALISMAN_FARMING_TEXTURE = "talisman_farming";
    public static final String TALISMAN_FARMING_UNLOCAL_NAME = "FarmingTalisman";
    public static final String TALISMAN_FARMING_NAME = "\u00a7dTalisman of Nourishment";

    public static int TALISMAN_HEAL_ID;
    public static final int TALISMAN_HEAL_DEFAULT_ID = 4404;
    public static final String TALISMAN_HEAL_TEXTURE = "talisman_heal";
    public static final String TALISMAN_HEAL_UNLOCAL_NAME = "HealTalisman";
    public static final String TALISMAN_HEAL_NAME = "\u00a7dTalisman of Healing";

    public static int TALISMAN_INVISIBLE_ID;
    public static final int TALISMAN_INVISIBLE_DEFAULT_ID = 4405;
    public static final String TALISMAN_INVISIBLE_TEXTURE = "talisman_invisible";
    public static final String TALISMAN_INVISIBLE_UNLOCAL_NAME = "InvisibleTalisman";
    public static final String TALISMAN_INVISIBLE_NAME = "\u00a7dTalisman of Vanishing";

    public static int TALISMAN_NETHER_ID;
    public static final int TALISMAN_NETHER_DEFAULT_ID = 4406;
    public static final String TALISMAN_NETHER_TEXTURE = "talisman_nether";
    public static final String TALISMAN_NETHER_UNLOCAL_NAME = "NetherTalisman";
    public static final String TALISMAN_NETHER_NAME = "\u00a7dTalisman of Inflammability";

    public static int TALISMAN_NIGHT_ID;
    public static final int TALISMAN_NIGHT_DEFAULT_ID = 4407;
    public static final String TALISMAN_NIGHT_TEXTURE = "talisman_night";
    public static final String TALISMAN_NIGHT_UNLOCAL_NAME = "NightTalisman";
    public static final String TALISMAN_NIGHT_NAME = "\u00a7dTalisman of the Night";

    public static int TALISMAN_REGEN_ID;
    public static final int TALISMAN_REGEN_DEFAULT_ID = 4408;
    public static final String TALISMAN_REGEN_TEXTURE = "talisman_regen";
    public static final String TALISMAN_REGEN_UNLOCAL_NAME = "RegenTalisman";
    public static final String TALISMAN_REGEN_NAME = "\u00a7dTalisman of Regeneration";

    public static int TALISMAN_RESISTANCE_ID;
    public static final int TALISMAN_RESISTANCE_DEFAULT_ID = 4409;
    public static final String TALISMAN_RESISTANCE_TEXTURE = "talisman_resistance";
    public static final String TALISMAN_RESISTANCE_UNLOCAL_NAME = "ResistanceTalisman";
    public static final String TALISMAN_RESISTANCE_NAME = "\u00a7dTalisman of Invincibility \u00a78(sort of)";

    public static int TALISMAN_SPEED_ID;
    public static final int TALISMAN_SPEED_DEFAULT_ID = 4410;
    public static final String TALISMAN_SPEED_TEXTURE = "talisman_speed";
    public static final String TALISMAN_SPEED_UNLOCAL_NAME = "SpeedTalisman";
    public static final String TALISMAN_SPEED_NAME = "\u00a7dTalisman of Acceleration";

    public static int TALISMAN_STRENGTH_ID;
    public static final int TALISMAN_STRENGTH_DEFAULT_ID = 4411;
    public static final String TALISMAN_STRENGTH_TEXTURE = "talisman_strength";
    public static final String TALISMAN_STRENGTH_UNLOCAL_NAME = "StrengthTalisman";
    public static final String TALISMAN_STRENGTH_NAME = "\u00a7dTalisman of Strength";

    public static int TALISMAN_WATER_ID;
    public static final int TALISMAN_WATER_DEFAULT_ID = 4412;
    public static final String TALISMAN_WATER_TEXTURE = "talisman_water";
    public static final String TALISMAN_WATER_UNLOCAL_NAME = "WaterTalisman";
    public static final String TALISMAN_WATER_NAME = "\u00a7dTalisman of Gillification";

    public static int TALISMAN_WITHER_ID;
    public static final int TALISMAN_WITHER_DEFAULT_ID = 4413;
    public static final String TALISMAN_WITHER_TEXTURE = "talisman_wither";
    public static final String TALISMAN_WITHER_UNLOCAL_NAME = "WitherTalisman";
    public static final String TALISMAN_WITHER_NAME = "\u00a7dTalisman of the Wither";

    public static int TALISMAN_INERT_ID;
    public static final int TALISMAN_INERT_DEFAULT_ID = 4414;
    public static final String TALISMAN_INERT_TEXTURE = "talisman_inert";
    public static final String TALISMAN_INERT_UNLOCAL_NAME = "InertTalisman";
    public static final String TALISMAN_INERT_NAME = "\u00a7dInert Talisman";
}
