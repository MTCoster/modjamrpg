package net.mtcoster.minecraft.rpgmod;

public class ModInfo {
    public static final String ID = "RpgMod";
    public static final String NAME = "RPG Mod";
    public static final String NETWORK_CHANNEL = "rpgmod";
    public static final String VERSION = "0.0.0";

    public static final String TAB_RPGMOD_ID = ID;
    public static final String TAB_RPGMOD_NAME = NAME;

    public static final String TAB_BLOCKS_ID = "ExtraBlocks";
    public static final String TAB_BLOCKS_NAME = "Extra Blocks";

    public static final String TEXTURE_LOCATION = ID.toLowerCase();
}
